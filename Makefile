CFLAGS := -Wall -Wextra -Wmissing-prototypes -pedantic
SRC := $(wildcard src/*.c)
OBJ := $(SRC:.c=.o)
TARGET := vic
LIB := -L/usr/lib -ltermbox
CC := cc

all: $(TARGET)

%.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@

$(TARGET): $(OBJ)
	$(CC) $(CFLAGS) $(OBJ) -o $(TARGET) $(LIB)

clean:
	rm -f $(TARGET) $(OBJ)
