#include "vic.h"

int inparse(char *s, char **t, int len)
{
	int i, j;

	for (i = 0; i < len; i++) {
		for (j = 0, s++; j < len && strchr(" \0", *s) == NULL; j++,s++)
			t[i][j] = *s;
		if (*s == '\0')
			break;
	}

	return i;
}
