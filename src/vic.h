enum {
	MAXPARAM = 15,
	MAXMSG = 512,
	MAXCHAN = 50,
	MAXLOG = 8192
};

struct irc {
	char *prefix, *command;
	char *params[MAXPARAM+1];
};

struct com {
	char *command;
	int (*fn) (struct irc *, int);
};

struct input {
	char *buf;
	int (*fn) (struct irc *, int);
};

struct target {
	int channel, last;
	char name[MAXCHAN+1], log[MAXLOG+1][MAXMSG+1];
};

/* addtarget: adds new target to targets, growing as needed */
int addtarget(int channel, char *name);

/* ircconnect: connect to host node with port service */
int ircconnect(const char *node, const char *service);

/* ircget: fill buf with data from fd until \r or \n */
int ircget(int fd, char *buf, int len);

/* ircidentify: identify on socket fd with nick nick and user user */
int ircidentify(int fd, char *nick, char *user);

/* ircpriv: send PRIVMSG to socket fd with target and message s */
int ircpriv(int fd, char *target, char *s);

/* inparse: put parameters from s to t with maximum length len */
int inparse(char *s, char **t, int len);

/* struct array of command handlers */
extern struct com commands[];

/* struct array of input command handlers */
extern struct input inputs[];

/* target growing array */
extern struct target *targets;

/* targets growing integer */
extern int lastt;
