#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/select.h>
#include <netdb.h>
#include <termbox.h>
#include <stdarg.h>
#include "vic.h"

#define PROGRAM "vic"
#define COMMAND '/'
#define FGBAR TB_DEFAULT
#define BGBAR TB_CYAN

enum {
	/* arguments */
	NODE = 1,
	SERVICE,
	NICK,
	USER,
	ARGMAX
};

struct key {
	int key, (*fn) (struct tb_event *, int);
};

static int lastn = 0;
/* current target */
static int curt = 0;
int lastt = -1;
/* initial maximum allocation */
static int targetmax = 8192;
/* message buffer */
static char mesg[8192] = { 0 };
struct target *targets;

int addtarget(int channel, char *name)
{
	if (++lastt >= targetmax) {
		targetmax *= 2;
		targets = realloc(targets, sizeof(struct target) * targetmax);
		if (targets == NULL)
			return -1;
	}

	targets[lastt].channel = channel;
	targets[lastt].last = 0;
	strcpy(targets[lastt].name, name);
	memset(targets[lastt].log, 0, sizeof targets[lastt].log);
	return 0;
}

static void freeall(struct irc *p)
{
	int i;

	free(p->prefix);
	free(p->command);
	for (i = 0; p->params[i] != NULL; i++)
		free(p->params[i]);
}

static char *sdup(const char *s)
{
	char *t;

	if ((t = malloc(strlen(s)+1)) == NULL)
		return NULL;
	strcpy(t, s);
	return t;
}

static int parse(struct irc *p, char *s)
{
	char pbuf[MAXMSG+1] = { 0 };
	char cbuf[MAXMSG+1] = { 0 };
	char pabuf[MAXMSG+1] = { 0 };
	size_t i, j;

	p->prefix = NULL;
	if (*s == ':') {
		for (i = 0, s++;i<sizeof pbuf&&strchr(" \0", *s)==NULL;i++,s++)
			pbuf[i] = *s;
		if (*s == '\0')
			return -1; /* not enough arguments */
		pbuf[++i] = '\0';
		if ((p->prefix = sdup(pbuf)) == NULL)
			return -2;
	}

	s += p->prefix != NULL;
	for (i = 0; i < sizeof cbuf && strchr(" \0", *s) == NULL; i++,s++)
		cbuf[i] = *s;
	if (*s == '\0')
		return -1;
	cbuf[++i] = '\0';
	if ((p->command = sdup(cbuf)) == NULL) {
		free(p->prefix);
		return -2;
	}

	for (i = 0; i < MAXPARAM; i++) {
		for (j=0,s++; j<sizeof pabuf&&strchr(" \0", *s)==NULL;j++,s++) {
			if (j == 0 && *s == ':')
				break;
			pabuf[j] = *s;
		}

		if (j == 0 && *s == ':' && (p->params[i] = sdup(++s)) != NULL) {
			break;
		} else if (p->params[i] == NULL) {
			free(p->prefix);
			free(p->command);
			return -2;
		}

		if ((p->params[i] = sdup(pabuf)) == NULL) {
			free(p->prefix);
			free(p->command);
			return -2;
		}

		if (*s == '\0')
			break;
		memset(pabuf, '\0', sizeof pabuf);
	}

	p->params[++i] = NULL;
	return 0;
}

static void tprint(int x, int y, uint16_t fg, uint16_t bg, const char *s)
{
	uint32_t uni;

	while (*s != '\0') {
		s += tb_utf8_char_to_unicode(&uni, s);
		tb_change_cell(x++, y, uni, fg, bg);
	}
}

static void tprintf(int x, int y, uint16_t fg, uint16_t bg, char *s, ...)
{
	char buf[8192];
	va_list vl;

	va_start(vl, s);
	vsprintf(buf, s, vl);
	va_end(vl);
	tprint(x, y, fg, bg, buf);
}

static void render(void)
{
	int i, j;

	tb_clear();
	for (i = tb_height()-3, j = targets[curt].last;; i--, j--) {
		if (i <= 0 || j <= 0)
			break;
		tprint(0, i, TB_BLACK, TB_DEFAULT, targets[curt].log[j]);
	}
	for (i = 0; i < tb_width(); i++)
		tb_change_cell(i, tb_height()-2, 0, FGBAR, BGBAR);
	tprintf(0, tb_height()-1, TB_BLACK, TB_DEFAULT, "%s", mesg);
	tb_set_cursor(lastn, tb_height()-1);
	tb_present();
}

static int enter(struct tb_event *ev, int fd)
{
	int i, max, args;
	char **p;

	max = 8192;
	if (mesg[0] == COMMAND) {
		if ((p = malloc(max * sizeof(char *))) == NULL)
			return -1;
		for (i = 0; i < max; i++)
			if ((p[i] = malloc(max * sizeof(char))) == NULL) {
				free(p);
				return -1;
			}
		args = inparse(s, t, max);
		for (i = 0; i < max; i++)
			free(p[i]);
		free(p);
		return 0;
	}

	if (ircpriv(fd, targets[curt].name, mesg) == -1)
		return -1;
	memset(mesg, 0, sizeof mesg);
	lastn = 0;
	return 0;
}

static int arrow(struct tb_event *ev, int fd)
{
	if ((ev->key == TB_KEY_ARROW_LEFT && lastn <= 0)
	|| (ev->key == TB_KEY_ARROW_RIGHT && lastn >= (int) strlen(mesg)))
		return -1;
	lastn -= (ev->key == TB_KEY_ARROW_LEFT) - \
		 (ev->key == TB_KEY_ARROW_RIGHT);
	return 0;
}

static int delete(struct tb_event *ev, int fd)
{
	if (lastn <= 0)
		return -1;
	lastn--;
	memmove(mesg+lastn, mesg+lastn+1, strlen(mesg));
	return 0;
}

static struct key keytab[] = {
	{ TB_KEY_ENTER, &enter },
	{ TB_KEY_ARROW_LEFT, &arrow },
	{ TB_KEY_ARROW_RIGHT, &arrow },
	{ 0x7f, &delete },
	{ -1, NULL }
};

static int irchandle(int fd)
{
	char buf[MAXMSG+1];
	struct irc msg;
	int i;

	memset(buf, 0, sizeof buf);
	if (ircget(fd, buf, sizeof buf) < 0)
		return -1;
	if (parse(&msg, buf) < 0)
		return 0;
	strcpy(targets[0].log[targets[0].last++], buf);
	render();
	for (i = 0; commands[i].command != NULL; i++)
		if (strcmp(commands[i].command, msg.command) == 0) {
			if (commands[i].fn(&msg, fd) == -1)
				return -1;
			break;
		}
	freeall(&msg);
	return 0;
}

static int inhandle(int fd)
{
	int i;
	struct tb_event ev;

	if (!tb_poll_event(&ev) || (ev.type == TB_EVENT_KEY
	&& ev.key == TB_KEY_CTRL_C))
		return -1;
	if (ev.type == TB_EVENT_RESIZE) {
		render();
		return 0;
	}
	for (i = 0; keytab[i].fn != NULL; i++)
		if (ev.key == keytab[i].key && keytab[i].fn(&ev, fd) == -1)
			return 0;
		else if (ev.key == keytab[i].key) {
			render();
			return 0;
		}
	if (lastn >= (int) sizeof mesg)
		return 0;
	mesg[lastn++] = ev.key == 0x20 ? ' ' : ev.ch;
	render();
	return 0;
}

int main(int argc, char *argv[])
{
	int fd, sel;
	fd_set fds;

	if (argc < ARGMAX) {
		fprintf(stderr, "usage: %s <node> <service> <nick> <user>\n",
			argv[0][0] == '\0' ? PROGRAM : argv[0]);
		return EXIT_FAILURE;
	}

	if (tb_init() < 0) {
		fprintf(stderr, "error: couldn't init termbox\n");
		return EXIT_FAILURE;
	}

	if ((fd = ircconnect(argv[NODE], argv[SERVICE])) == -1) {
		tb_shutdown();
		fprintf(stderr, "error: unable to connect\n");
		return EXIT_FAILURE;
	}

	if (!ircidentify(fd, argv[NICK], argv[USER])) {
		tb_shutdown();
		fprintf(stderr, "error: could not identify with IRC\n");
		close(fd);
		return EXIT_FAILURE;
	}

	if (((targets = malloc(targetmax * sizeof(struct target))) == NULL)
	|| (addtarget(0, "") == -1)) {
		tb_shutdown();
		fprintf(stderr, "error: could not allocate memory for"
				"target\n");
		close(fd);
		return EXIT_FAILURE;
	}

	render();
	for (;;) {
		FD_ZERO(&fds);
		FD_SET(fd, &fds);
		FD_SET(fileno(stdin), &fds);

		if ((sel = select(fd+1, &fds, 0, 0, NULL)) == -1) {
			free(targets);
			tb_shutdown();
			fprintf(stderr, "error: cannot multiplex descriptors"
					"\n");
			close(fd);
			return EXIT_FAILURE;
		}

		if (FD_ISSET(fd, &fds) && (irchandle(fd) == -1))
			break;
		if (FD_ISSET(fileno(stdin), &fds) && (inhandle(fd) == -1))
			break;
	}

	free(targets);
	tb_shutdown();
	close(fd);
	return 0;
}
