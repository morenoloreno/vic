#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>
#include "vic.h"

int ircconnect(const char *node, const char *service)
{
	int fd;
	struct addrinfo *res, *p;
	struct addrinfo hints = { 0 };

	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;

	if (getaddrinfo(node, service, &hints, &res) != 0)
		return -1;
	for (p = res; p != NULL; p = p->ai_next) {
		fd = socket(p->ai_family, p->ai_socktype, p->ai_protocol);
		if (fd == -1)
			continue;
		if (connect(fd, p->ai_addr, p->ai_addrlen) == -1) {
			close(fd);
			continue;
		}
		freeaddrinfo(res);
		return fd;
	}

	freeaddrinfo(res);
	return -1;
}

int ircget(int fd, char *buf, int len)
{
	int c, i;

	for (i = 0; i < len;) {
		if (recv(fd, &c, 1, 0) <= 0)
			return -1;
		if (strchr("\n\r", c) != NULL)
			break;
		buf[i++] = c;
	}

	buf[++i] = '\0';
	return 0;
}

static int ircping(struct irc *p, int fd)
{
	int i;
	char msg[MAXMSG+1];

	i = sprintf(msg, "PONG :%s\r\n", p->params[0]);
	return write(fd, msg, i) == i;
}

static int irc001(struct irc *p, int fd)
{
	return 1;
}

static int ircjoin(struct irc *p, int fd)
{
	return !addtarget(1, p->params[0]);
}

static int ircprivmsg(struct irc *p, int fd)
{
	int i;
	char s[8192];

	p->prefix[strcspn(p->prefix, "!")] = '\0';
	sprintf(s, "<%s> %s\n", p->prefix, p->params[1]);
	for (i = 0; i < lastt; i++)
		if (strcmp(p->params[0], targets[i].name) == 0) {
			strcpy(targets[i].log[targets[i].last++], s);
			break;
		}
	return 0;
}

int ircidentify(int fd, char *nick, char *user)
{
	int i;
	char msg[MAXMSG+1];

	i = sprintf(msg, "NICK %s\r\nUSER %s 0 * :realname\r\n",
		    nick, user);
	return write(fd, msg, i) == i;
}

int ircpriv(int fd, char *target, char *s)
{
	int i;
	char msg[MAXMSG+1];

	i = sprintf(msg, "PRIVMSG %s :%s\r\n", target, s);
	return write(fd, msg, i) == i;
}

struct com commands[] = {
	{ "PING", &ircping },
	{ "001", &irc001 },
	{ "JOIN", &ircjoin },
	{ NULL, NULL }
};
